# About this project.

My CV in HTML/CSS. It could serve as basis for a CV somewhere on a future website.
For now, I just use a browser snapshot tool to create a PDF from the in-browser result.

# About React

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
