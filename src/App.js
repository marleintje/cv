import { Col, Container, Row } from 'react-bootstrap';
import './data/FontAwesomeIcons';
import IconTextList from './components/IconTextList';
import Section from './components/Section';
import JobBlock from './components/JobBlock';
import TechnicalSkillset from './components/TechnicalSkillset';
import Courses from './components/Courses';
import Education from './components/Education';
import Languages from './components/Languages';
import {
	contactInfo,
	courses,
	education,
	hobbies,
	restJobs,
	devJobs,
	languageSet,
	otherTechnicalSkills,
	technicalSkills
} from './data/info';
import ImageMarlein from './data/me.jpg'
require('./styling/index.scss');

function App() {
	return (
		<Container>
			<div className='py-5' />
			<Row>
				<Col xs={4}>
					<div className='py-5 d-flex align-items-center h-100 px-8'>
						<img style={{borderRadius: '10px', maxHeight: '250px'}} className='mw-100' alt='self-portrait' src={ImageMarlein} />
					</div>
				</Col>
				<Col xs={8}>
					<div className='position-relative bg-primary text-white p-5 rounded shadow'>
						<h1 className='fw-500'>Marlein Rusch</h1>
						<h2 className='border-bottom pb-2 mb-4 d-inline-block border-3'>
							Medior/Senior front-end developer
						</h2>
						<p className='mb-0' style={{ fontSize: '22px' }}>
							Organised, pragmatic and enthusiastic front-end developer with 5.5 years of coding experience. I have an eye for detail and a structured approach to design-to-code conversion. My technical strengths lie in responsive coding, bootstrap (config), React + Next JS.
						</p>
					</div>
				</Col>
			</Row>
			<Row className='mt-7'>
				<Col xs={4}>
					<div className='ps-3'>
						<IconTextList list={contactInfo} />
					</div>
					<Section className='mt-6' isLeftColumn title='Technical skills'>
						<p><em className='text-black'>Relative to my own understanding on the topics. I do not claim to master HTML and JS a 100%.</em></p>
						<TechnicalSkillset otherTechnicalSkills={otherTechnicalSkills} technicalSkills={technicalSkills} />
					</Section>
					<Section isLeftColumn title='Languages'>
						<Languages languageSet={languageSet} />
					</Section>
					<Section isLeftColumn title='Interests'>
						<IconTextList iconIsExtraLarge list={hobbies} />
					</Section>
				</Col>
				<Col xs={8} className='description'>
					<Section title='Work experience'>
						<JobBlock jobs={devJobs} />
						<JobBlock isVaria jobs={restJobs} />
					</Section>
					<Section title='Courses and Trainings'>
						<Courses courses={courses} />
					</Section>
					<Section title='Education'>
						<Education education={education} />
					</Section>
				</Col>
			</Row>
		</Container>

	);
}

export default App;
