const parseMarkdownLinks = (text) => {
	const regex = /\[(.*?)\]\((.*?)\)/g;
	const parts = text.split(regex);
  
	return parts.map((part, index) => {
	  if (index % 3 === 1) {
		// Render the link text
		const linkText = parts[index];
		return (
		  <a
			key={index}
			href={parts[index + 1]}
			target="_blank"
			rel="noopener noreferrer"
		  >
			{linkText}
		  </a>
		);
	  } else if (index % 3 === 0) {
		// Render plain text
		return <span key={index}>{part}</span>;
	  } else {
		// Render the link destination (URL)
		return null;
	  }
	});
};
  
export default parseMarkdownLinks