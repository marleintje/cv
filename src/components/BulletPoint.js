import React from 'react';
import parseMarkdownLinks from '../parseMarkdownLinks';

const BulletPoint = ({text}) => <li className='position-relative ps-6'>
	<span className='bullet-point-red' />
	<span className='bullet-point-white' />
	{parseMarkdownLinks(text)}
</li>


export default BulletPoint;
