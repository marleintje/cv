const Education = ({education}) => <div>
	{education.map((course, i) => <div key={i}>
		<h3 className='mb-1'>{course.title}</h3>
		<h4 className='mb-0'>{course.institution}</h4>
		<p className='d-flex justify-content-between small mb-3'>
			<span><em>{course.dateRange}</em></span>
			<span><em>{course.footnote}</em></span>
		</p>
	</div>)}
</div>

export default Education;