import { Table } from "react-bootstrap"

const Languages = ({ languageSet }) =><Table className='table-sm' borderless>
	<tbody>
		{languageSet.map((language, i) =><tr key={i}>
			<th className='w-50'>{language.name}</th>
			<td className='w-50'>{language.proficiency}</td>
		</tr>)}
	</tbody>
</Table>


export default Languages;
