import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import parseMarkdownLinks from "../parseMarkdownLinks";

const IconTextList = ({ list, iconIsExtraLarge = false}) => <div className='icon-text-list'>
	{list.map((item, i) => <div className='d-flex align-items-center my-3' key={i}>
		<span className='text-center' style={{width: '25px'}}>
			<FontAwesomeIcon icon={item.icon} className={`text-primary ${iconIsExtraLarge ? 'extra-': ''}large text-center`} />
		</span>
		<p className='ms-4 mb-0'>{parseMarkdownLinks(item.text)}</p>
	</div>)}
</div>

export default IconTextList;
