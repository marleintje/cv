const Section = ({ className = '', isLeftColumn = false, children, title }) => <section className={`mb-7 ${className}`}>
	<h2 className={`text-uppercase ${isLeftColumn ? 'ps-4': ''}`}>{title}</h2>
	<hr className='mt-3'/>
	<div className={isLeftColumn ? 'ps-4': ''}>
		{children}
	</div>
</section>

export default Section;