import BulletPoint from "./BulletPoint";

const JobBlock = ({ jobs, isVaria = false }) => isVaria ? <div>
	<h3 className='mb-1'>Varia</h3>
	{jobs.map((item, i) => <div key={i} className='pb-2'>
		<div className="d-flex justify-content-between">
			<h4 className='mb-2'>{item.jobTitle} - {item.company}</h4>
			<div className='small'>
				<span><em>{item.dateRange}, {item.location}</em></span>
			</div>
		</div>
		
		<ul className='list-unstyled'>
			{item.tasks.map((task, i) => <BulletPoint key={i} text={task}/>)}
		</ul>
	</div>)}
</div> : <div>
	{jobs.map((item, i) => <div key={i} className='pb-2'>
		<h3 className='mb-1'>{item.jobTitle}</h3>
		<h4 className='mb-0'>{item.company}</h4>
		<p className='d-flex justify-content-between small mb-3'>
			<span><em>{item.dateRange}</em></span>
			<span><em>{item.location}</em></span>
		</p>
		<ul className='list-unstyled'>
			{item.tasks.map((task, i) => <BulletPoint key={i} text={task}/>)}
		</ul>
	</div>)}
</div>

export default JobBlock;