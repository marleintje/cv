import BulletPoint from "./BulletPoint";

const TechnicalSkillset = ({ otherTechnicalSkills, technicalSkills }) => <div className='contact-info-list'>
	{technicalSkills.map((skill, i) => <div key={i}>
			<p className='mb-1 mt-4 fw-bold'>{skill.text}</p>
			<div style={{height: '14px'}} className='border border-primary border-2 position-relative'>
				<div
					className='position-absolute bg-primary'
					style={{top: '-1px', left: '-1px', height: '12px', width: `${skill.level + 1}%`}}
				/>
			</div>
	</div>)}
	<h4 className='mt-4'>Varia:</h4>
	<ul className='list-unstyled'>
		{otherTechnicalSkills.map((skill, i) => <BulletPoint key={i} text={skill} />)}
	</ul>
</div>

export default TechnicalSkillset;