const Courses = ({ courses }) => <>
	{courses.map((course, i) => <div className="d-flex justify-content-between align-items-center mb-3" key={i}>
		<div>

		{
			course.titles.map((title, i) => <h4 key={i} className='mb-0'>{title}</h4>)
		}
		</div>
		<div>
			<span><em>{course.dateRange},</em> {course.institution}</span>
		</div>
	</div>)}
</>

export default Courses;
