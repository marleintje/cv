
import { library } from '@fortawesome/fontawesome-svg-core';
import {
	faEnvelope,
	faMapMarkerAlt,
	faMitten,
	faMobileAlt,
	faMusic,
	faTools,
} from '@fortawesome/free-solid-svg-icons';

import {
	faGitlab,
	faLinkedin,
} from '@fortawesome/free-brands-svg-icons';

export const fontAwesomeIconsLibrary = library.add(
	faEnvelope,
	faGitlab,
	faLinkedin,
	faMapMarkerAlt,
	faMitten,
	faMobileAlt,
	faMusic,
	faTools,
);

export default fontAwesomeIconsLibrary;
