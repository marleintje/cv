const contactInfo = [
	{
		icon: 'envelope',
		text: 'marleinrusch@gmail.com'
	},
	{
		icon: 'mobile-alt',
		text: '00316 16 15 8668'
	},
	{
		icon: 'map-marker-alt',
		text: 'Bloemendaal'
	},
	{
		icon: ['fab', 'gitlab'],
		text: '[https://gitlab.com/marleintje](https://gitlab.com/marleintje)'
	},
	{
		icon: ['fab', 'linkedin'],
		text: '[https://linkedin.com/in/marlein-rusch](https://linkedin.com/in/marlein-rusch)'
	},
]

const hobbies = [
	{
		icon: 'mitten',
		text: 'Sewing'
	},
	{
		icon: 'music',
		text: 'Dancing'
	},
	{
		icon: 'tools',
		text: 'DIY'
	},
]

const courses = [
	{
		titles: [
			'Professional Scrum Master I',
		],
		institution: 'Scrum.org',
		dateRange: '2023'
	},
	{
		titles: [
			'Introduction to SQL',
			'Search Engine Optimization Fundamentals',
			'Building web applications in PHP'
		],
		institution: 'Coursera ∙ Online',
		dateRange: '2019/2020'
	},
	{
		titles: ['3-month full stack web development course'],
		institution: 'Codaisseur ∙ Amsterdam',
		dateRange: '2018'
	}
]

const education = [
	{
		title: 'M.A. Linguistics',
		institution: 'Université libre de Bruxelles (ULB) ∙ Belgium',
		dateRange: '2014 - 2016',
		footnote: 'Education in French'
	},
	{
		title: 'B.S. Sociology',
		institution: 'Vrije Universiteit Brussel (VUB) ∙ Belgium',
		dateRange: '2009 - 2013',
		footnote: 'Education in Dutch'
	},
	{
		title: 'Erasmus Programme Exchange',
		institution: 'Universität Leipzig ∙ Germany',
		dateRange: 'Mar - Jul 2021',
		footnote: 'Education in German'
	},
]

const devJobs = [
	{
		jobTitle: 'Front-end developer',
		company: 'Vision Healthcare (Flinndal)',
		dateRange: '01/01/2019 to present',
		location: 'Haarlem',
		tasks: [
			'Create new website login-portal, multi-company + multi-language. React/Redux application from scratch with HTML5, SCSS/bootstrap, JS/React/Redux',
			'Complete restyling for 5 websites, ([flinndal.nl](https://flinndal.nl), [purasana.fr](https://purasana.com), etc) 1 codebase. CShtml/ JS/JQuery',
			'Pixel-perfect design, exact copy of old website ([vitaminexpress.org](https://vitaminexpress.org)). NextJS / GraphQL',
			'Storefront project, modular design fits-all idea ([baerbel-drexel.de](https://baerbel-drexel.de)). NextJS / TypeScript'
		]
	},
	{
		jobTitle: 'Front-end freelancer',
		company: 'Rusch Webservices / Paratroopers.dev',
		dateRange: '01/10/2021 to present, 1 day a week.',
		location: 'Haarlem',
		tasks: [
			'[Fullstack custom webshop](https://zingen-en-stembevrijding.nl/), paid project. Self hosted Strapi backend, NextJS frontend. Multi-language set-up. Custom payment flow with Mollie and webhooks in Strapi, Mailchimp newsletter signups, automated emailing through Sendgrid.',
			'[Simple custom webshop](https://potloodenpenseel.nl/), hobby project. Hygraph / NextJS / Bootstrap / Netlify / simple self-created Figma design.',
			'[Online Pictionary](https://imaginary.marlein.dev/), silly side project. Including a <canvas> element.',
		]
	}
]

const restJobs = [
	{
		jobTitle: 'Performance Executive',
		company: 'BookingSuite (Booking.com)',
		dateRange: '2017 - 2018',
		location: 'Amsterdam',
		tasks: [
			'Advise and educate partners on products and SEO, DNS and CMS related matters, in FR, EN, NL',
			'Cooperate with product teams and propose new product features and processes'
		]
	},
	{
		jobTitle: 'Student Assistent',
		company: 'Université Libre de Bruxelles',
		dateRange: '2016',
		location: 'Brussels, Belgium',
		tasks: [
			'Prepare and teach practical classes for bachelor Linguistics course',
		]
	},
	{
		jobTitle: 'EU Procurement Intern',
		company: 'PwC',
		dateRange: '2016',
		location: 'Brussels, Belgium',
		tasks: [
			'Support of tender submission and contract preparation and quality control',
			'Selection and circulation of business opportunities to the PwC EU Services Network',
		]
	},
]

const languageSet = [
	{
		name: 'NL',
		proficiency: 'Mother tongue'
	},
	{
		name: 'EN, FR, SP',
		proficiency: 'Fluent'
	},
	{
		name: 'DE',
		proficiency: 'Proficient'
	},
]

const otherTechnicalSkills = [
	'Hygraph, Strapi',
	'Styled components, tailwind',
	'Scrum/Agile',
	'Adobe Lightroom, Audition, Indesign',
	'Restful APIs',
	'SPSS Statistics',
	'LaTeX',
	'Inkscape',
	'Docker',
	'Git'
]

const technicalSkills = [
	{
		text: 'HTML5 / SCSS',
		level: 100
	},
	{
		text: 'JavaScript',
		level: 100
	},
	{
		text: 'React / NextJS',
		level: 95
	},
	{
		text: 'GraphQL',
		level: 85
	},
	{
		text: 'TypeScript',
		level: 70
	},
	{
		text: 'Jquery',
		level: 60
	},
	{
		text: 'SQL',
		level: 55
	},
	{
		text: 'Figma',
		level: 50
	},
	{
		text: 'Python',
		level: 25
	},
	{
		text: 'PHP',
		level: 20
	},
]

export {
	contactInfo,
	courses,
	devJobs,
	education,
	hobbies,
	languageSet,
	otherTechnicalSkills,
	restJobs,
	technicalSkills,
}